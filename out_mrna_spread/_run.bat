@echo off

echo. > _results.txt

FOR %%i IN (dir /s/b *_nw.txt) DO ( 
	echo %%i
	type %%i > intree
	echo ; >> intree
	type _reftree.txt >> intree
	
	echo %%i >> _results.txt
	
	_treedist < _cmd_kf.txt
	findstr Trees outfile >> _results.txt
	_treedist < _cmd_rf.txt
	findstr Trees outfile >> _results.txt

	
	echo. >> _results.txt
	)

pause
